function openList(){
    var map = document.getElementById('map');
    var listBtn = document.getElementById('listBtn');
    var mapBtn = document.getElementById('mapViewBtn');
    var listView = document.getElementById('listView');

    map.style.display = "none";
    listBtn.style.display = "none";
    mapBtn.style.display = "block";
    listView.style.display = "block";

}


function closeList(){
    var map = document.getElementById('map');
    var listBtn = document.getElementById('listBtn');
    var mapBtn = document.getElementById('mapViewBtn');
    var listView = document.getElementById('listView');

    map.style.display = "block";
    listBtn.style.display = "block";
    mapBtn.style.display = "none";
    listView.style.display = "none";

}

function backToTop(){
    window.scrollTo(0,0);
}