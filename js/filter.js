var filter = document.getElementById('filterToggle');
var searchbar = document.getElementById('searchBar');
var bodyPart = document.getElementById('bodyPart');

function showFilter(){
    filter.style.display = "block";
    searchbar.style.display = "none";
    bodyPart.style.display = "none";
}

function closeFilter(){
    filter.style.display = "none";
    searchbar.style.display = "block";
    bodyPart.style.display = "block";
}
   
    

var btns = document.getElementsByClassName("filter-selection-btn");
for (var i = 0; i < btns.length; i++) {
    btns[i].addEventListener("click", function() {
        
        if(this.parentNode.id != "commute"){
            var current = this.parentNode.getElementsByClassName("active");
        
            if(current.length != 0)  {
                current[0].className = current[0].className.replace(" active", "");
            }
            
            this.className += " active";
        }
        else{
            this.classList.toggle("active");
        }
        
});
}

var header = document.getElementById("statHeader")
var furnishedStatus = header.getElementsByClassName("pdt-10 pdb-10 width-33 text-center")
for (var i = 0; i < furnishedStatus.length; i++) {
    furnishedStatus[i].addEventListener("click", function() {
        
        
        var current = this.parentNode.getElementsByClassName("thm-bg rm-border white-text fw-600");
        
            if(current.length != 0)  {
                current[0].className = current[0].className.replace(" thm-bg rm-border white-text fw-600", "");
            }
            
         this.className += " thm-bg rm-border white-text fw-600";
        
       
        
});
}

function addTick(item) {
    var child = item.getElementsByTagName('i')[0];
    if(child.className == "fa fa-square-o font-26 pdl-10"){
        child.className = "fa fa-check-square thm-text font-26 pdl-10";
    }
    else{
        child.className = "fa fa-square-o font-26 pdl-10";
    }

}


function clearSelection(id){
    var header = document.getElementById(id);
    var current = header.getElementsByClassName("active");
    if(current.length != 0){
        current[0].className = current[0].className.replace(" active", "");
    }
}




    
function reset(){
    clearSelection("propertyType");
    clearSelection("roomType");
    clearSelection("preference");
    
    document.getElementById('myRange').value='0';
    document.getElementById('fromSlider').value='0';
    document.getElementById('toSlider').value='4000';

    var commute = document.getElementById('commute');
   
    
    var current = commute.getElementsByClassName("active");  
    
    if(current.length != 0)  {
        for(let i =0; i<current.length;i+1){
            current[i].className = current[i].className.replace(" active", "");
        }
        
    }

    var count = document.getElementsByClassName("fa fa-check-square thm-text font-26 pdl-10");
    if(count.length != 0)  {
        for(let j =0; j<count.length;j+1){
          
            count[j].className = "fa fa-square-o font-26 pdl-10";
        }
        
    }

    var count2 = document.getElementsByClassName("thm-bg rm-border white-text fw-600");
    if(count2.length != 0)  {
        for(let j =0; j<count2.length;j+1){
          
            count2[j].className = "pdt-10 pdb-10 width-33 text-center";
        }
        
    }

    document.getElementById('roomNum').selectedIndex = 0;
    document.getElementById('sqftInput').value='';
    
}

